# alpine-base
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-base)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-base)



----------------------------------------
### x86_64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-base/x64)
### aarch64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-base/aarch64)
### armv7
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-base/aarch64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64,armv7
* Appplication : -



----------------------------------------
#### Run

```sh
docker run -i -t --rm \
           forumi0721/alpine-base:[ARCH_TAG]
```



----------------------------------------
#### Usage

```dockerfile
FROM forumi0721/alpine-base:[ARCH_TAG]

RUN 'build-code'
```



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

